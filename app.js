const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');

const app = express();
app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(express.static('public'));

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/signup.html');
});

app.post('/', (req, res) => {
	var fname = req.body.fname;
	var lname = req.body.lname;
	var email = req.body.email;

	var data = {
		members: [
			{
				email_address: email,
				status: 'subscribed',
				merge_fields: {
					FNAME: fname,
					LNAME: lname
				}
			}
		]
	};

	var jsonData = JSON.stringify(data);
	var option = {
		url: 'https://us4.api.mailchimp.com/3.0/lists/0055e5c31f',
		method: 'POST',
		headers: {
			Authorization: 'Anish1 6e47e2ae9d8410d1493dbceb6f6bc4cc-us4'
		},
		body: jsonData
	};
	request(option, (err, response, body) => {
		if (err) {
			res.sendFile(__dirname + '/failur.html');
		} else {
			if (response.statusCode === 200) {
				res.sendFile(__dirname + '/success.html');
			} else {
				res.sendFile(__dirname + '/failur.html');
			}
		}
	});
	// res.send("WE hit the post route");
});

app.listen(3000, () => {
	console.log('Server is running');
});

// 6e47e2ae9d8410d1493dbceb6f6bc4cc-us4

//List ID
//
